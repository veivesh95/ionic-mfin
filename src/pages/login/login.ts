import { Component } from '@angular/core';
import { IonicPage,AlertController, NavController, NavParams, LoadingController,Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { HomePage } from "../../pages/home/home";

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
   loading: Loading;
  registerCredentials = { username: '', password: '' };

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthService, private alertCtrl: AlertController,private alertCtrl1: AlertController, private loadingCtrl: LoadingController) {
    this.navCtrl = navCtrl;
}

   showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  public login() {
    this.showLoading()
    this.auth.login(this.registerCredentials).subscribe(allowed => {
      if (allowed) {        
        this.navCtrl.setRoot(HomePage);
      } else {
        this.showError("Access Denied");
      }
    },
      error => {
        this.showError(error);
      });
  }

  forgot() {
    let alert = this.alertCtrl.create({
      title: 'Forgot login detail!',
      message: 'Contact your Head Office representative with your work ID.',
      buttons: ['Ok']
    });
    alert.present()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

}
