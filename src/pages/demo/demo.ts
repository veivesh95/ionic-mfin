import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Demo page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-demo',
  templateUrl: 'demo.html',
})
export class Demo {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Demo');
  }

  slides = [
    {
      title: "Welcome to the Guide!",
      description: "<b>The Guide will show a number of useful components that are useful.</b>",
       image: "assets/phone.png",
    },
    {
      title: "What is Micro-Finance?",
      description: "<b>Microfinance is a source of financial services for entrepreneurs and small businesses lacking access to banking and related services.</b>",
       image: "assets/mob.jpg",
    }
  ];

  finish(){
    this.navCtrl.push('HomePage');
  }

  skip(){
    this.navCtrl.push('HomePage');
  }

}
