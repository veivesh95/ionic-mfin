import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Loansummary } from './loansummary';

@NgModule({
  declarations: [
    Loansummary,
  ],
  imports: [
    IonicPageModule.forChild(Loansummary),
  ],
  exports: [
    Loansummary
  ]
})
export class LoansummaryModule {}
