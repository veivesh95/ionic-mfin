import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Elout } from './elout';

@NgModule({
  declarations: [
    Elout,
  ],
  imports: [
    IonicPageModule.forChild(Elout),
  ],
  exports: [
    Elout
  ]
})
export class EloutModule {}
