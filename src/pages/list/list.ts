import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { RestAPIservice } from "../../providers/rest-ap-iservice";
import { CustomerView } from "../customer-view/customer-view";  

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  // selectedItem: any;
  // icons: string[];
  // items: Array<{title: string, note: string, icon: string}>;

  users: any;
  id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restCalls: RestAPIservice) {
    this.getUsers();
  }

  getUsers(){
    this.restCalls.getUsers()
    .then(data => {
      this.users = data;
      console.log(this.users);
    });
  }

  openContactPage(user){
    this.id = user;
    console.log("Button check --> object passing? "+this.id);
    this.navCtrl.push(CustomerView, {
      customer: user
    });
  }

  
}

    // // If we navigated to this page, we will have an item available as a nav param
    // this.selectedItem = navParams.get('item');

    // // Let's populate this page with some filler content for funzies
    // this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    // 'american-football', 'boat', 'bluetooth', 'build'];

    // this.items = [];
    // for (let i = 1; i < 11; i++) {
    //   this.items.push({
    //     title: 'Item ' + i,
    //     note: 'This is item #' + i,
    //     icon: this.icons[Math.floor(Math.random() * this.icons.length)]
    //   });
    // }
//  }

//  itemTapped(event, item) {
 //   // That's right, we're pushing to ourselves!
  //  this.navCtrl.push(ListPage, {
 //     item: item
 //   });
// }