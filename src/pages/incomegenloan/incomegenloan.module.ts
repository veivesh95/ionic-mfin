import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Incomegenloan } from './incomegenloan';

@NgModule({
  declarations: [
    Incomegenloan,
  ],
  imports: [
    IonicPageModule.forChild(Incomegenloan),
  ],
  exports: [
    Incomegenloan
  ]
})
export class IncomegenloanModule {}
