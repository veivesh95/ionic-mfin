import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ilout } from './ilout';

@NgModule({
  declarations: [
    Ilout,
  ],
  imports: [
    IonicPageModule.forChild(Ilout),
  ],
  exports: [
    Ilout
  ]
})
export class IloutModule {}
