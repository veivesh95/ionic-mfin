import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ilreq } from './ilreq';

@NgModule({
  declarations: [
    Ilreq,
  ],
  imports: [
    IonicPageModule.forChild(Ilreq),
  ],
  exports: [
    Ilreq
  ]
})
export class IlreqModule {}
