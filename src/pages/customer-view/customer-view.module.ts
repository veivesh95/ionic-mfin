import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerView } from './customer-view';

@NgModule({
  declarations: [
    CustomerView,
  ],
  imports: [
    IonicPageModule.forChild(CustomerView),
  ],
  exports: [
    CustomerView
  ]
})
export class CustomerViewModule {}
