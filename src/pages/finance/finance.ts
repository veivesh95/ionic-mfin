import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CashCollection} from '../cash-collection/cash-collection';
import {Loanhome} from '../loanhome/loanhome';
/**
 * Generated class for the Finance page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-finance',
  templateUrl: 'finance.html',
})
export class Finance {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Finance');
  }

   cashcollect()
  {
    this.navCtrl.push(CashCollection);
  }

  viewLoan()
  {
    this.navCtrl.push(Loanhome);
  }

}
