import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mtlreq } from './mtlreq';

@NgModule({
  declarations: [
    Mtlreq,
  ],
  imports: [
    IonicPageModule.forChild(Mtlreq),
  ],
  exports: [
    Mtlreq
  ]
})
export class MtlreqModule {}
