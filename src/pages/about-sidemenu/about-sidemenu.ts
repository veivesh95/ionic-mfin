import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { About} from '../about/about';
/**
 * Generated class for the AboutSidemenu page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-about-sidemenu',
  templateUrl: 'about-sidemenu.html',
})
export class AboutSidemenu {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutSidemenu');
  }

 /* callAbout()
  {
    this.navCtrl.push(About);
  }*/

}
