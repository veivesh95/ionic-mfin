# Micro Finance Mobile Application #

This application is developed with ionic framework by using HTML5, AngularJS, TypeScript and Javascript.
Only focuses on the User interface and user experience of the application.
The data tier is maintained by another GIT source and it will be used for the development procedures separately. 

Coding environment followed through-out the development 

* Apache Cordova CLI - 4.2.0
* Ionic CLI - 2.2.3
* Ionic App Lib Version - 2.2.1
* Node Version - 7.9.0

Visual Studio Code as the primary IDE used to code this application

#### Personnel Details ####

Product Owner - Mr. Chathuranga Manamendra (chathuranga.it@gmail.com)

Dev Team
* Veivesh K. 
* Divvyashini G.
* Kaushekan R.
* Sriram A.

Project Supervisor - Ms. Geethanjali Wimalaratne

*For any contact and feedbacks, veivesh95@gmail.com*

## How to use this project

### With the Ionic CLI:

open the project folder through Windows explorer or directory and `ionic serve` to run the project in the web browser - browser view.

### Pre-requisites
* NodeJS      v7.10.0
* NPM         v4.2.0
* Ionic CLI   v3.4.0
* Cordova     v7.0.1
 * Cordova-Android v6.2.2

and Android Studio along with Android SDK and Adroid Virtual Device Manager or Genymotion for emulating the application in a mobile environment.

Then, to run it, cd into project folder and run:

```bash
$ npm install  
$ ionic cordova platform add android@6.4.0
$ ionic cordova emulate android
```

or to run with browser,

```bash
$ npm install
$ ionic serve
```

*Substitute android for ios if not on a Mac.*

