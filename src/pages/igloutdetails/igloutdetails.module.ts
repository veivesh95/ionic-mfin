import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Igloutdetails } from './igloutdetails';

@NgModule({
  declarations: [
    Igloutdetails,
  ],
  imports: [
    IonicPageModule.forChild(Igloutdetails),
  ],
  exports: [
    Igloutdetails
  ]
})
export class IgloutdetailsModule {}
