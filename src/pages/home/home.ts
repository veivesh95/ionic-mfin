import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,ModalController} from 'ionic-angular';
import { Login } from "../../pages/login/login";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,public navParams: NavParams, public toastCtrl: ToastController,public modalCtrl: ModalController) {

  }

  logout(){
    this.navCtrl.setRoot(Login);
  }

  showToast(position: string) {
    let toast = this.toastCtrl.create({
      message: 'Today Cash Collection from Kamal in Bambalapitiya',
      duration: 5000,
      position: position
    });

    toast.present(toast);
  }

  feedback(){
    this.navCtrl.push('Feedback');
  }

  intro(){
    this.navCtrl.push('Demo');
  }

  history(){
    this.navCtrl.push('History');
  }

  openModal(){
    this.navCtrl.push('About');
  }

}
