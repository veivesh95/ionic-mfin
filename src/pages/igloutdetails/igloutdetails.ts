import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the Igloutdetails page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-igloutdetails',
  templateUrl: 'igloutdetails.html',
})
export class Igloutdetails {

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Igloutdetails');
  }
  
    showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Alert',
      subTitle: 'Payment Received, Database Updated Successfully',
      buttons: ['OK']
    });
    alert.present();
  }

}
