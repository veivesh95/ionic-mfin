import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Messaging } from './messaging';

@NgModule({
  declarations: [
    Messaging,
  ],
  imports: [
    IonicPageModule.forChild(Messaging),
  ],
  exports: [
    Messaging
  ]
})
export class MessagingModule {}
