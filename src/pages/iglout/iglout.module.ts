import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Iglout } from './iglout';

@NgModule({
  declarations: [
    Iglout,
  ],
  imports: [
    IonicPageModule.forChild(Iglout),
  ],
  exports: [
    Iglout
  ]
})
export class IgloutModule {}
