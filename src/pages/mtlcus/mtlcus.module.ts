import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mtlcus } from './mtlcus';

@NgModule({
  declarations: [
    Mtlcus,
  ],
  imports: [
    IonicPageModule.forChild(Mtlcus),
  ],
  exports: [
    Mtlcus
  ]
})
export class MtlcusModule {}
