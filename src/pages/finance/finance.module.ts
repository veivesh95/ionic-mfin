import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Finance } from './finance';

@NgModule({
  declarations: [
    Finance,
  ],
  imports: [
    IonicPageModule.forChild(Finance),
  ],
  exports: [
    Finance
  ]
})
export class FinanceModule {}
