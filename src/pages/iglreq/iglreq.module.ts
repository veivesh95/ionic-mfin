import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Iglreq } from './iglreq';

@NgModule({
  declarations: [
    Iglreq,
  ],
  imports: [
    IonicPageModule.forChild(Iglreq),
  ],
  exports: [
    Iglreq
  ]
})
export class IglreqModule {}
