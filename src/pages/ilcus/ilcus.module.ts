import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ilcus } from './ilcus';

@NgModule({
  declarations: [
    Ilcus,
  ],
  imports: [
    IonicPageModule.forChild(Ilcus),
  ],
  exports: [
    Ilcus
  ]
})
export class IlcusModule {}
