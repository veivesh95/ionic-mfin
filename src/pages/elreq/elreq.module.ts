import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Elreq } from './elreq';

@NgModule({
  declarations: [
    Elreq,
  ],
  imports: [
    IonicPageModule.forChild(Elreq),
  ],
  exports: [
    Elreq
  ]
})
export class ElreqModule {}
