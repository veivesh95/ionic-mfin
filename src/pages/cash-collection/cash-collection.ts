import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CollectionDetails} from '../collection-details/collection-details';
import {Back} from '../back/back';
import {Next} from '../next/next';

/**
 * Generated class for the CashCollection page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-cash-collection',
  templateUrl: 'cash-collection.html',
})
export class CashCollection {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CashCollection');
  }

    details()
  {
    this.navCtrl.push(CollectionDetails);
  }

   back()
  {
    this.navCtrl.push(Back);
  }

   next()
  {
    this.navCtrl.push(Next);
  }


}
