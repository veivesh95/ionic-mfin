import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Iglreqdetails} from '../iglreqdetails/iglreqdetails';
/**
 * Generated class for the Iglreq page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-iglreq',
  templateUrl: 'iglreq.html',
})
export class Iglreq {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Iglreq');
  }

  viewreqdetails()
{
  this.navCtrl.push(Iglreqdetails);
  
}

}
