import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Loanhome } from './loanhome';

@NgModule({
  declarations: [
    Loanhome,
  ],
  imports: [
    IonicPageModule.forChild(Loanhome),
  ],
  exports: [
    Loanhome
  ]
})
export class LoanhomeModule {}
