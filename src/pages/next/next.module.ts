import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Next } from './next';

@NgModule({
  declarations: [
    Next,
  ],
  imports: [
    IonicPageModule.forChild(Next),
  ],
  exports: [
    Next
  ]
})
export class NextModule {}
