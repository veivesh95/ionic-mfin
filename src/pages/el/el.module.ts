import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { El } from './el';

@NgModule({
  declarations: [
    El,
  ],
  imports: [
    IonicPageModule.forChild(El),
  ],
  exports: [
    El
  ]
})
export class ElModule {}
