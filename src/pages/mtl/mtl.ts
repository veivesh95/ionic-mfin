import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Mtlcus} from '../mtlcus/mtlcus';
import {Mtlout} from '../mtlout/mtlout';
import {Mtlreq} from '../mtlreq/mtlreq';
/**
 * Generated class for the Mtl page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-mtl',
  templateUrl: 'mtl.html',
})
export class Mtl {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Mtl');
  }

  mtlcuscall(){
  this.navCtrl.push(Mtlcus);
}

mtloutcall(){
  this.navCtrl.push(Mtlout);
}

mtlreqcall()
{
  this.navCtrl.push(Mtlreq);
}

}
