import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Iglcus } from './iglcus';

@NgModule({
  declarations: [
    Iglcus,
  ],
  imports: [
    IonicPageModule.forChild(Iglcus),
  ],
  exports: [
    Iglcus
  ]
})
export class IglcusModule {}
