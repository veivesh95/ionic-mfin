import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutSidemenu } from './about-sidemenu';

@NgModule({
  declarations: [
    AboutSidemenu,
  ],
  imports: [
    IonicPageModule.forChild(AboutSidemenu),
  ],
  exports: [
    AboutSidemenu
  ]
})
export class AboutSidemenuModule {}
