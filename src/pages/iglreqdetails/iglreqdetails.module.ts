import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Iglreqdetails } from './iglreqdetails';

@NgModule({
  declarations: [
    Iglreqdetails,
  ],
  imports: [
    IonicPageModule.forChild(Iglreqdetails),
  ],
  exports: [
    Iglreqdetails
  ]
})
export class IglreqdetailsModule {}
