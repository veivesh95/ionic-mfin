import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CashCollection } from './cash-collection';

@NgModule({
  declarations: [
    CashCollection,
  ],
  imports: [
    IonicPageModule.forChild(CashCollection),
  ],
  exports: [
    CashCollection
  ]
})
export class CashCollectionModule {}
