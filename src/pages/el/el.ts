import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Elcus} from '../elcus/elcus';
import {Elout} from '../elout/elout';
import {Elreq} from '../elreq/elreq';
/**
 * Generated class for the El page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-el',
  templateUrl: 'el.html',
})
export class El {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad El');
  }

  elcuscall(){
    this.navCtrl.push(Elcus);
  }

  eloutcall(){
   this.navCtrl.push(Elout);
 }

 elreqcall()
 {
   this.navCtrl.push(Elreq);

 }

}
