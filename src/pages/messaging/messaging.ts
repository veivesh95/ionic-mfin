import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { SMS } from "@ionic-native/sms";

/**
 * Generated class for the Messaging page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-messaging',
  templateUrl: 'messaging.html',
})
export class Messaging {

  mobileNumber: number;
  textMessage: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public toastCtrl: ToastController, private smsCtrl: SMS) {
    this.mobileNumber = Number(navParams.get('phoneNumber'));
  }

  ionViewDidLoad() {
    console.log('\n Messaging page has been opened\n');
  }

  async sendTextMessage(){
    console.log("\n Send message button has been clicked");

    try{
      await this.smsCtrl.send(String(this.mobileNumber), this.textMessage);
      const toastCtrl = this.toastCtrl.create({
        message: 'Text has been sent!',
        duration: 2000,
        position: "top"
      });

      toastCtrl.onDidDismiss(()=> {
        console.log("\n Toast dismissed\n");
      });

      toastCtrl.present();
    }
      catch (excep){
        const toastCtrlCatch = this.toastCtrl.create({
          message: 'Text has been sent',
          //duration: 2000,
          position: "top",
          showCloseButton: true,
          closeButtonText: "OK"
        });

        toastCtrlCatch.onDidDismiss(()=> {
        console.log("\n ToastCatch dismissed\n");
      });

        toastCtrlCatch.present();
      }
    }

    closeModal(){
      this.viewCtrl.dismiss();
    }

}
