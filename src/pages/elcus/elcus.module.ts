import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Elcus } from './elcus';

@NgModule({
  declarations: [
    Elcus,
  ],
  imports: [
    IonicPageModule.forChild(Elcus),
  ],
  exports: [
    Elcus
  ]
})
export class ElcusModule {}
