import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Il } from './il';

@NgModule({
  declarations: [
    Il,
  ],
  imports: [
    IonicPageModule.forChild(Il),
  ],
  exports: [
    Il
  ]
})
export class IlModule {}
