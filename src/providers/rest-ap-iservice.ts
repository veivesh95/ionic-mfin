import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class RestAPIservice {

  data: any;
  customerId: number;
  apiUri = 'http://mfin-api-167306.appspot.com';

  constructor(public http: Http) {
    console.log('Rest API Service has been called');
  }

  //gets all the customer JSON objects from the API
  getUsers() {
    if (this.data) {
     return Promise.resolve(this.data);
    }
    
    return new Promise(resolve => {
      this.http.get(this.apiUri+'/getCustomers')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  //save data by post (i guess, this is a sample format)
  saveUser(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUri+'/getCustomers', JSON.stringify(data))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

}
