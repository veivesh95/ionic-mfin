import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Ilcus} from '../ilcus/ilcus';
import {Ilout} from '../ilout/ilout';
import{Ilreq} from '../ilreq/ilreq';
/**
 * Generated class for the Il page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-il',
  templateUrl: 'il.html',
})
export class Il {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Il');
  }

  ilcuscall()
  {
  this.navCtrl.push(Ilcus);
}

iloutcall()
{
  this.navCtrl.push(Ilout);

}

ilreqcall()
{
  this.navCtrl.push(Ilreq);
}

}
