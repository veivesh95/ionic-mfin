Micro Finance Mobile application using Ionic 2 and Cordova Native integ.

## How to use this template

### With the Ionic CLI:

```bash
$ npm install  
$ ionic cordova platform add android@6.4.0
$ ionic cordova emulate android
```

Or to run with browser,

```bash
$ npm install
$ ionic serve
```
