import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the CollectionDetails page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-collection-details',
  templateUrl: 'collection-details.html',
})
export class CollectionDetails {

 /* constructor(public navCtrl: NavController, public navParams: NavParams) {
}*/
constructor(private alertCtrl: AlertController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CollectionDetails');
  }

  displayMsg()
 {
    let alert = this.alertCtrl.create({
    title: 'Confirm Payment',
    message: 'Do you want enter this amount and continue?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Confirm',
        handler: () => {
          console.log('Confirm clicked');
        }
      }
    ]
  });
  alert.present();
 }

 checkSMS()
 { 
   let prompt = this.alertCtrl.create({
      title: 'Mobile Number',
      message: "Enter the number of the Customer to send the receipt as a text message ",
      inputs: [
        {
          name: 'title',
          placeholder: 'Enter Number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
          }
        }
      ]
    });
    prompt.present();
  
 }

 checkEmail()
 {
   let prompt = this.alertCtrl.create({
      title: 'Email Address',
      message: "Enter the Email address of the Customer to send the receipt ",
      inputs: [
        {
          name: 'title',
          placeholder: 'Enter Email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
          }
        }
      ]
    });
    prompt.present();
 }


 checkPrReceipt()
 {
   let alert = this.alertCtrl.create({
    title: 'Confirm Print Receipt',
    message: 'Do you want to use the bluetooth printer? ',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Confirm',
        handler: () => {
          console.log('Confirm clicked');
        }
      }
    ]
  });
  alert.present();
 
 }


}
