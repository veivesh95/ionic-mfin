import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CollectionDetails } from './collection-details';

@NgModule({
  declarations: [
    CollectionDetails,
  ],
  imports: [
    IonicPageModule.forChild(CollectionDetails),
  ],
  exports: [
    CollectionDetails
  ]
})
export class CollectionDetailsModule {}
