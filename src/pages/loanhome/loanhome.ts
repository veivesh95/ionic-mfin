import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Incomegenloan} from '../incomegenloan/incomegenloan';
import {Mtl} from '../mtl/mtl';
import {El} from '../el/el';
import {Il} from '../il/il';
import {Loansummary} from '../loansummary/loansummary'; 

/**
 * Generated class for the Loanhome page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-loanhome',
  templateUrl: 'loanhome.html',
})
export class Loanhome {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Loanhome');
  }

  calligl()
  {
  this.navCtrl.push(Incomegenloan);

  }

  callmtl()
  {
  this.navCtrl.push(Mtl);
  }

  callel()
  {
  this.navCtrl.push(El);
}

callil()
{
  this.navCtrl.push(Il);
}

loansumm()
{
  this.navCtrl.push(Loansummary);
}

}
