import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController } from 'ionic-angular';
import { RestAPIservice } from "../../providers/rest-ap-iservice";
import { Messaging } from "../messaging/messaging";
import { CallNumber } from "@ionic-native/call-number";

/**
 * Generated class for the CustomerView page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-customer-view',
  templateUrl: 'customer-view.html',
})
export class CustomerView {

  guy: any;
  mobileNo: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restCalls: RestAPIservice, public modalCtrl: ModalController, private callCtrl: CallNumber) {
    this.guy = navParams.get('customer');
    this.mobileNo = Number(this.guy.phone);
  }

  ionViewDidLoad() {
    console.log('\n Customer Page has been opened\n');
  }

  openMessage(getNumber){
    let modal = this.modalCtrl.create(Messaging, {
      phoneNumber: getNumber
    });
    modal.present();

    console.log("Passed parameter: "+getNumber);
  }

  async callNumber():Promise<any>{
    try{
      await this.callCtrl.callNumber(String(this.mobileNo),true);
    }
    catch (excep){
      console.error(excep);
    }
  }
}
