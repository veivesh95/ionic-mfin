import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mtlout } from './mtlout';

@NgModule({
  declarations: [
    Mtlout,
  ],
  imports: [
    IonicPageModule.forChild(Mtlout),
  ],
  exports: [
    Mtlout
  ]
})
export class MtloutModule {}
