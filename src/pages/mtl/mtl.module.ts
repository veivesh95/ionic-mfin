import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mtl } from './mtl';

@NgModule({
  declarations: [
    Mtl,
  ],
  imports: [
    IonicPageModule.forChild(Mtl),
  ],
  exports: [
    Mtl
  ]
})
export class MtlModule {}
