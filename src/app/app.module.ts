import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from "@angular/http";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {Finance} from '../pages/finance/finance';
import { CallNumber } from "@ionic-native/call-number";
import { SMS } from "@ionic-native/sms";


import {CashCollection} from '../pages/cash-collection/cash-collection';
import {CollectionDetails} from '../pages/collection-details/collection-details';
import {Back} from '../pages/back/back';
import {Next} from '../pages/next/next';
//import {About} from '../pages/about/about';
import { AuthService } from "../providers/auth-service";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Loanhome} from '../pages/loanhome/loanhome';
import {Incomegenloan} from '../pages/incomegenloan/incomegenloan';
import {Iglcus} from '../pages/iglcus/iglcus';
import {Iglout} from '../pages/iglout/iglout';
import {Iglreq} from '../pages/iglreq/iglreq';
import {Igloutdetails} from '../pages/igloutdetails/igloutdetails';
import {Iglreqdetails} from '../pages/iglreqdetails/iglreqdetails';
import {Mtl} from '../pages/mtl/mtl';
import { Mtlcus} from '../pages/mtlcus/mtlcus';
import {Mtlout} from '../pages/mtlout/mtlout';
import {Mtlreq} from '../pages/mtlreq/mtlreq';
import {El} from '../pages/el/el';
import {Elcus} from '../pages/elcus/elcus';
import {Elout} from '../pages/elout/elout';
import {Elreq} from '../pages/elreq/elreq';
import {Il} from '../pages/il/il';
import {Ilcus} from '../pages/ilcus/ilcus';
import {Ilout} from '../pages/ilout/ilout';
import{Ilreq} from '../pages/ilreq/ilreq';
import {Loansummary} from '../pages/loansummary/loansummary';
import { RestAPIservice } from "../providers/rest-ap-iservice";
import { CustomerView } from "../pages/customer-view/customer-view";
import { Messaging } from "../pages/messaging/messaging";

import { Login } from "../pages/login/login";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    Finance,
    
    CashCollection,
    CollectionDetails,
    Back,
    Next,
    Loanhome,
    Incomegenloan,
    Iglcus,
    Iglout,
    Iglreq,
    Igloutdetails,
    Iglreqdetails,
    Login,
    Mtl,
    Mtlcus,
    Mtlout,
    Mtlreq,
    El,
    Elcus,
    Elout,
    Elreq,
    Il,
    Ilcus,
    Ilout,
    Ilreq,
    Loansummary,
    CustomerView,
    Messaging

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    Finance,
    
    CashCollection,
    CollectionDetails,
    Back,
    Next,
    Loanhome,
    Incomegenloan,
    Iglcus,
    Iglout,
    Iglreq,
    Igloutdetails,
    Iglreqdetails,
    Login,
    Mtl,
    Mtlcus,
    Mtlout,
    Mtlreq,
    El,
    Elcus,
    Elout,
    Elreq,
    Il,
    Ilcus,
    Ilout,
    Ilreq,
    Loansummary,
    CustomerView,
    Messaging

  ],
  providers: [
    StatusBar,
    SplashScreen,
    RestAPIservice,
    // User,
    AuthService,
    CallNumber,
    SMS,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
