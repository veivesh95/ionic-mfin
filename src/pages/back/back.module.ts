import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Back } from './back';

@NgModule({
  declarations: [
    Back,
  ],
  imports: [
    IonicPageModule.forChild(Back),
  ],
  exports: [
    Back
  ]
})
export class BackModule {}
