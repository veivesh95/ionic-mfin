import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Igloutdetails} from '../igloutdetails/igloutdetails';
/**
 * Generated class for the Iglout page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-iglout',
  templateUrl: 'iglout.html',
})
export class Iglout {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Iglout');
  }

  showdetails()
  {
    this.navCtrl.push(Igloutdetails);
  }

}
