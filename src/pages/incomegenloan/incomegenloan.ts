import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Iglcus} from '../iglcus/iglcus';
import {Iglout} from '../iglout/iglout';
import {Iglreq} from '../iglreq/iglreq';
/**
 * Generated class for the Incomegenloan page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-incomegenloan',
  templateUrl: 'incomegenloan.html',
})
export class Incomegenloan {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Incomegenloan');
  }

  iglcuscall()
  {
  this.navCtrl.push(Iglcus);
  
  }

  igloutcall()
  {
  this.navCtrl.push(Iglout);

  }

  iglreqcall()
  {
  this.navCtrl.push(Iglreq);
  
  }
}
