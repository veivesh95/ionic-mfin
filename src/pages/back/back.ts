import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{CollectionDetails} from '../collection-details/collection-details';
import {CashCollection} from '../cash-collection/cash-collection';
/**
 * Generated class for the Back page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-back',
  templateUrl: 'back.html',
})
export class Back {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Back');
  }

   details(){
    this.navCtrl.push(CollectionDetails)
  }

  viewCC(){
    this.navCtrl.push(CashCollection);
  }


}
